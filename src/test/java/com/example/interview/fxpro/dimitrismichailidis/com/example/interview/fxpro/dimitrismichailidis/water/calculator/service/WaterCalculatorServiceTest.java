package com.example.interview.fxpro.dimitrismichailidis.com.example.interview.fxpro.dimitrismichailidis.water.calculator.service;

import com.example.interview.fxpro.dimitrismichailidis.water.calculator.service.WaterCalculatorService;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.junit.runner.RunWith;

/**
 * JUnit class to test WaterCalculatorService
 * */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WaterCalculatorServiceTest.WaterCalculatorServiceTestConfiguration.class)
public class WaterCalculatorServiceTest {

    @Autowired
    WaterCalculatorService waterCalculatorService;

    /**
     * Test findNextLeftBankOfLake method
     * */
    @Test
    public void findNextDescendingIndexTestFirstLeftBank(){
        Assert.assertNotNull(waterCalculatorService);

        // Given
        int startingPoint = 0;
        int expectedValue = 0;
        int[] landScape = WaterCalculatorTestingUtils.getArraySameAsInPDF();

        // Check
        int actualValue =  waterCalculatorService.findNextLeftBankOfLake(startingPoint, landScape);

        // Assert
        Assert.assertEquals("PDF exercise first left bank found" , expectedValue, actualValue);
    }

    /**
     * Test findNextLeftBankOfLake method
     * */
    @Test
    public void findNextDescendingIndexTestSecondLeftBank(){
        Assert.assertNotNull(waterCalculatorService);

        // Given
        int startingPoint = 4;
        int expectedValue = 4;
        int[] landScape = WaterCalculatorTestingUtils.getArraySameAsInPDF();

        // Check
        int actualValue =  waterCalculatorService.findNextLeftBankOfLake(startingPoint, landScape);

        // Assert
        Assert.assertEquals("PDF exercise second left bank found" , expectedValue, actualValue);
    }


    /**
     * Test findNextRightBankOfLake method
     * */
    @Test
    public void findFirstRightBankOfLakeFromPDFExercise(){
        Assert.assertNotNull(waterCalculatorService);

        // Given
        int startingPoint = 0;
        int expectedValue = 4;
        int[] landScape = WaterCalculatorTestingUtils.getArraySameAsInPDF();

        // Check
        int actualValue = waterCalculatorService.findNextRightBankOfLake(startingPoint, landScape);

        // Assert
        Assert.assertEquals("PDF exercise first right bank found" , expectedValue, actualValue);
    }

    /**
     * Test findNextRightBankOfLake method
     * */
    @Test
    public void findSecondRightBankOfLakeFromPDFExercise(){
        Assert.assertNotNull(waterCalculatorService);

        // Given
        int startingPoint = 4;
        int expectedValue = 7;
        int[] landScape = WaterCalculatorTestingUtils.getArraySameAsInPDF();

        // Check
        int actualValue =  waterCalculatorService.findNextRightBankOfLake(startingPoint, landScape);

        // Assert
        Assert.assertEquals("PDF exercise first right bank found" , expectedValue, actualValue);
    }


    /**
     * Test calculateSingleLakeWaterAmount method
     * */
    @Test
    public void calculateSingleLakeWaterAmountFirstTest(){
        Assert.assertNotNull(waterCalculatorService);

        // Given
        int leftBankOfLake = 0;
        int rightBankOfLake = 4;
        int expectedValue = 6;
        int[] landScape = WaterCalculatorTestingUtils.getArraySameAsInPDF();

        // Check
        int actualValue =  waterCalculatorService.calculateSingleLakeWaterAmount(leftBankOfLake, rightBankOfLake, landScape);

        // Assert
        Assert.assertEquals("PDF exercise first lake size check" , expectedValue, actualValue);

    }

    /**
     * Test calculateSingleLakeWaterAmount method again
     * */
    @Test
    public void calculateSingleLakeWaterAmountSecondTest(){
        Assert.assertNotNull(waterCalculatorService);

        // Given
        int leftBankOfLake = 4;
        int rightBankOfLake = 7;
        int expectedValue = 3;
        int[] landScape = WaterCalculatorTestingUtils.getArraySameAsInPDF();

        // Check
        int actualValue =  waterCalculatorService.calculateSingleLakeWaterAmount(leftBankOfLake, rightBankOfLake, landScape);

        // Assert
        Assert.assertEquals("PDF exercise second lake size check" , expectedValue, actualValue);

    }

    /**
     * Configuration to declare needed bean for JUnit
     * */
    @Configuration
    public static class WaterCalculatorServiceTestConfiguration {
        @Bean
        public WaterCalculatorService waterCalculatorService (){
            return new WaterCalculatorService();
        }
    }

}
