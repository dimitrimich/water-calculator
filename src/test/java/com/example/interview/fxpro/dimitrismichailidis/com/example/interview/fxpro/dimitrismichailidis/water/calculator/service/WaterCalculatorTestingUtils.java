package com.example.interview.fxpro.dimitrismichailidis.com.example.interview.fxpro.dimitrismichailidis.water.calculator.service;

import java.util.Random;

/**
 * Utilities used for testing
 *
 * */
public class WaterCalculatorTestingUtils {

    /**
     * Get a "landscape" like the exercise's
     *
     * */
    public static int[] getArraySameAsInPDF(){
        int[] landscape = new int[9];

        landscape[0] = 5;
        landscape[1] = 2;
        landscape[2] = 3;
        landscape[3] = 4;
        landscape[4] = 5;
        landscape[5] = 4;
        landscape[6] = 0;
        landscape[7] = 3;
        landscape[8] = 1;
        return landscape;
    }

    /**
     * Get a "landscape" with a "U" shape
     *
     * */
    public static int[] getArrayLikeBucket(){
        int[] landscape = new int[9];

        landscape[0] = 5;
        landscape[1] = 0;
        landscape[2] = 0;
        landscape[3] = 0;
        landscape[4] = 1;
        landscape[5] = 0;
        landscape[6] = 0;
        landscape[7] = 0;
        landscape[8] = 5;

        return landscape;

    }

    /**
     * Get a "landscape" with shape as below
     *
     *         .
     *       . .   .
     *     . . .   .
     *     . . .   .   .
     * . . . . . . . . .
     *
     * */
    public static int[] getArrayNo3(){
        int[] landscape = new int[9];

        landscape[0] = 1;
        landscape[1] = 1;
        landscape[2] = 3;
        landscape[3] = 4;
        landscape[4] = 5;
        landscape[5] = 1;
        landscape[6] = 4;
        landscape[7] = 1;
        landscape[8] = 2;
        return landscape;
    }

    /**
     * Get a "landscape" with shape as below
     *
     * . . . . . . . . .
     * . . . . . . . . .
     * . . . . . . . . .
     * . . . . . . . . .
     * . . . . . . . . .
     *
     * */
    public static int[] getArrayNo4(){
        int[] landscape = new int[9];

        landscape[0] = 5;
        landscape[1] = 5;
        landscape[2] = 5;
        landscape[3] = 5;
        landscape[4] = 5;
        landscape[5] = 5;
        landscape[6] = 5;
        landscape[7] = 5;
        landscape[8] = 5;
        return landscape;
    }

    /**
     * Get a "landscape" without land
     * */
    public static int[] getArrayNo5(){
        int[] landscape = new int[9];

        landscape[0] = 0;
        landscape[1] = 0;
        landscape[2] = 0;
        landscape[3] = 0;
        landscape[4] = 0;
        landscape[5] = 0;
        landscape[6] = 0;
        landscape[7] = 0;
        landscape[8] = 0;
        return landscape;
    }

    /**
     * Get a "landscape" with shape as below
     * (top line missing)
     *
     * . . . . . . . . .
     * . . . . . . . . .
     * . . . . . . . . .
     * . . . . . . . . .
     *
     * */
    public static int[] getArrayNo6(){
        int[] landscape = new int[9];

        landscape[0] = 4;
        landscape[1] = 4;
        landscape[2] = 4;
        landscape[3] = 4;
        landscape[4] = 4;
        landscape[5] = 4;
        landscape[6] = 4;
        landscape[7] = 4;
        landscape[8] = 4;
        return landscape;
    }


    /**
     * Get a "landscape" with shape as below
     *
     *         .   . . .
     * .       . . . . .
     * . .     . . . . .
     * . . .   . . . . .
     * . . .   . . . . .
     *
     * */
    public static int[] getArrayNo7(){
        int[] landscape = new int[9];

        landscape[0] = 4;
        landscape[1] = 3;
        landscape[2] = 2;
        landscape[3] = 0;
        landscape[4] = 5;
        landscape[5] = 4;
        landscape[6] = 5;
        landscape[7] = 5;
        landscape[8] = 5;
        return landscape;
    }


    /**
     * Get a "landscape" with shape as below
     *
     * .
     * . . . .
     * . . . . . .
     * . . . . . .
     * . . . . . .
     * */
    public static int[] getArrayNo8(){
        int[] landscape = new int[9];

        landscape[0] = 5;
        landscape[1] = 4;
        landscape[2] = 4;
        landscape[3] = 4;
        landscape[4] = 3;
        landscape[5] = 3;
        landscape[6] = 0;
        landscape[7] = 0;
        landscape[8] = 0;
        return landscape;
    }


    /**
     * Get a "landscape" with shape as below
     *
     *           .
     *     .     .
     * .   .     .
     * .   .     .
     * . . . . . .
     * */
    public static int[] getArrayNo9(){
        int[] landscape = new int[9];

        landscape[0] = 3;
        landscape[1] = 1;
        landscape[2] = 4;
        landscape[3] = 1;
        landscape[4] = 1;
        landscape[5] = 5;
        landscape[6] = 0;
        landscape[7] = 0;
        landscape[8] = 0;
        return landscape;
    }


    /**
     * Get a "landscape" with negative Value
     * */
    public static int[] getIllegalArray(){
        int[] landscape = new int[9];

        landscape[0] = -123;
        landscape[1] = 1;
        landscape[2] = -23432;
        landscape[3] = 1;
        landscape[4] = 1;
        landscape[5] = 5;
        landscape[6] = 0;
        landscape[7] = 0;
        landscape[8] = 0;
        return landscape;
    }

    /**
     * Get a "landscape" with height bigger than allowed
     * */
    public static int[] getIllegalArrayBiggerHeightThanAllowed(){
        int[] landscape = new int[9];

        landscape[0] = 2;
        landscape[1] = 1;
        landscape[2] = 32001;
        landscape[3] = 1;
        landscape[4] = 1;
        landscape[5] = 5;
        landscape[6] = 0;
        landscape[7] = 0;
        landscape[8] = 0;
        return landscape;
    }

    /** A random landscape with 9 places and max height of 5 */
    public static int[] getRandomArray(){
        int[] landscape = new int[9];

        landscape[0] = new Random().nextInt(5);
        landscape[1] = new Random().nextInt(5);
        landscape[2] = new Random().nextInt(5);
        landscape[3] = new Random().nextInt(5);
        landscape[4] = new Random().nextInt(5);
        landscape[5] = new Random().nextInt(5);
        landscape[6] = new Random().nextInt(5);
        landscape[7] = new Random().nextInt(5);
        landscape[8] = new Random().nextInt(5);

        return landscape;

    }

    /** A random landscape with 32000 places and max height of 32000 */
    public static int[] getRandomArray32000(){
        int[] landscape = new int[32000];

        for(int i=0; i<32000 ; i++){
            landscape[i] = new Random().nextInt(32000);
        }

        return landscape;

    }
}
