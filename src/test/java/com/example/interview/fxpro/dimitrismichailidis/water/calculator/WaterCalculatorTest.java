package com.example.interview.fxpro.dimitrismichailidis.water.calculator;

import com.example.interview.fxpro.dimitrismichailidis.com.example.interview.fxpro.dimitrismichailidis.water.calculator.service.WaterCalculatorTestingUtils;
import com.example.interview.fxpro.dimitrismichailidis.water.calculator.service.WaterCalculatorService;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * JUnit class to test WaterCalculatorService
 * */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WaterCalculatorTest.WaterCalculatorTestConfiguration.class)
public class WaterCalculatorTest {

    @Autowired
    IWaterCalculator waterCalculator;

    /** Number of random checks */
    private final int NUMBER_OF_RANDOM_RUNS = 30;

    /** Testing total water amount retrieval in dummy landscapes*/
    @Test
    public void testGetAmountOfWater() {

        long waterAMountLandScape1 = waterCalculator.calculateWaterAmount(WaterCalculatorTestingUtils.getArraySameAsInPDF());
        Assert.assertEquals(9, waterAMountLandScape1);

        long waterAMountLandScape2 = waterCalculator.calculateWaterAmount(WaterCalculatorTestingUtils.getArrayLikeBucket());
        Assert.assertEquals(34, waterAMountLandScape2);

        long waterAMountLandScape3 = waterCalculator.calculateWaterAmount(WaterCalculatorTestingUtils.getArrayNo3());
        Assert.assertEquals(4, waterAMountLandScape3);

        long waterAMountLandScape4 = waterCalculator.calculateWaterAmount(WaterCalculatorTestingUtils.getArrayNo4());
        Assert.assertEquals(0, waterAMountLandScape4);

        long waterAMountLandScape5 = waterCalculator.calculateWaterAmount(WaterCalculatorTestingUtils.getArrayNo5());
        Assert.assertEquals(0, waterAMountLandScape5);

        long waterAMountLandScape6 = waterCalculator.calculateWaterAmount(WaterCalculatorTestingUtils.getArrayNo6());
        Assert.assertEquals(0, waterAMountLandScape6);

        long waterAMountLandScape7 = waterCalculator.calculateWaterAmount(WaterCalculatorTestingUtils.getArrayNo7());
        Assert.assertEquals(8, waterAMountLandScape7);

        long waterAMountLandScape8 = waterCalculator.calculateWaterAmount(WaterCalculatorTestingUtils.getArrayNo8());
        Assert.assertEquals(0, waterAMountLandScape8);

        long waterAMountLandScape9 = waterCalculator.calculateWaterAmount(WaterCalculatorTestingUtils.getArrayNo9());
        Assert.assertEquals(8, waterAMountLandScape9);

    }

    /** Testing total water amount retrieval in dummy landscapes
     * Should throw Exception as we are sending illegal height ( negative height) */
    @Test(expected = IllegalArgumentException.class)
    public void testValidationOfGetAmountOfWater() {
        waterCalculator.calculateWaterAmount(WaterCalculatorTestingUtils.getIllegalArrayBiggerHeightThanAllowed());
    }

    /** Testing total water amount retrieval in dummy landscapes
     * Should throw Exception as we are sending illegal height (bigger height than 32000 ) */
    @Test(expected = IllegalArgumentException.class)
    public void testValidationOfGetAmountOfWaterBiggerHeightThanAllowed() {
        waterCalculator.calculateWaterAmount(WaterCalculatorTestingUtils.getIllegalArray());
    }

    /** Testing total water amount retrieval in random landscapes
     * to get any unexpected errors
     * */
    @Test
    public void testGetAmountOfWaterAtRandomLandscapes() {
        for (int i = 0; i < NUMBER_OF_RANDOM_RUNS; i ++){
            waterCalculator.calculateWaterAmount(WaterCalculatorTestingUtils.getRandomArray());
        }
    }

    /** Testing total water amount retrieval in random landscapes
     * to get any unexpected errors.
     * Using maximum number of places to check for out-of-index and such errors
     * */
    @Test
    public void testGetAmountOfWaterAtHugeRandomLandscapes() {
        for (int i = 0; i < NUMBER_OF_RANDOM_RUNS; i ++){
            waterCalculator.calculateWaterAmount(WaterCalculatorTestingUtils.getRandomArray32000());
        }
    }

    /**
     * Configuration to declare needed bean for JUnit
     * */
    @Configuration
    public static class WaterCalculatorTestConfiguration {
        @Bean
        public WaterCalculatorService waterCalculatorService (){
            return new WaterCalculatorService();
        }
        @Bean
        public WaterCalculator waterCalculator() {
            return new WaterCalculator();
        }

    }
}
