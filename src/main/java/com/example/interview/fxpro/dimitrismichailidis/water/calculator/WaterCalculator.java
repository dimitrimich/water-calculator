package com.example.interview.fxpro.dimitrismichailidis.water.calculator;

import com.example.interview.fxpro.dimitrismichailidis.water.calculator.service.WaterCalculatorService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The WaterCalculator implements IWaterCalculator
 * */
@Component
public class WaterCalculator implements IWaterCalculator{

    @Autowired
    WaterCalculatorService waterCalculatorService;

    /**  Calculates the water amount of a given landscape
     * @param landscape an array of integers representing the landscape
     * @return a long value that represents tha amount of water in square spaces
     * @return -1 in case for some reason we end up an in infinite loop and exit from that due to safetyCheck*/
    @Override
    public long calculateWaterAmount(int[] landscape) {

        waterCalculatorService.validateLandscape(landscape);

        int currentIndex = 0;
        long totalWaterAmount = 0;
        int numberOfIterations = 0;

        // Iterates indefinetely until code can not find a left bank or a right bank
        // Bank represent the side of the imaginary "Lake" that holds water in landscape
        while(numberOfIterations < Integer.MAX_VALUE){
            // find the left bank of an imaginary lake on the landscape
            int leftBank = waterCalculatorService.findNextLeftBankOfLake(currentIndex, landscape);
            if(leftBank < 0){
                return totalWaterAmount;
            }
            currentIndex = leftBank;

            // find the right bank of an imaginary lake on the landscape
            int rightBank = waterCalculatorService.findNextRightBankOfLake(currentIndex, landscape);
            if(rightBank < 0){
                return totalWaterAmount;
            }

            // find the amount of water between left and right bank
            totalWaterAmount += waterCalculatorService.calculateSingleLakeWaterAmount(leftBank, rightBank, landscape);

            currentIndex = rightBank;

            // increase counter as a fail-safe
            numberOfIterations++;
        }

        return -1;
    }
}
