package com.example.interview.fxpro.dimitrismichailidis.water.calculator;

/** Interface used as contract to implement a way to calculate water amount in given landscape
 * */
public interface IWaterCalculator {

    long calculateWaterAmount(int[] landscape);

}
