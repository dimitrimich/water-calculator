package com.example.interview.fxpro.dimitrismichailidis.water.calculator.entity;

/**
 * Custom Data Transfer Object to exchange the errors messages
 *
 */
public class ResponseDTO {
    /** Error message if any */
    private String errorMessage;
    /** Expected water amount if no error */
    private long waterAmount;

    public ResponseDTO(String errorMessage){
        this.errorMessage = errorMessage;
    }
    public ResponseDTO(long waterAmount){
        this.waterAmount = waterAmount;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public long getWaterAmount() {
        return waterAmount;
    }

    public void setWaterAmount(long waterAmount) {
        this.waterAmount = waterAmount;
    }
}
