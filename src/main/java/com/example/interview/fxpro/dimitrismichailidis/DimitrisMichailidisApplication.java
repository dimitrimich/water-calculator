package com.example.interview.fxpro.dimitrismichailidis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DimitrisMichailidisApplication {

	public static void main(String[] args) {
		SpringApplication.run(DimitrisMichailidisApplication.class, args);
	}

}
