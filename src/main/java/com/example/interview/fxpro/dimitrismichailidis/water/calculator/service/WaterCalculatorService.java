package com.example.interview.fxpro.dimitrismichailidis.water.calculator.service;

import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.OptionalInt;
import java.util.stream.IntStream;

/** WaterCalculatorService offers methods to help calculate water level
 * Considering water will be lockated in "lakes" we are playing with
 * metaphor of "left bank" and "right bank" of lake
 * "Bank" positions do not hold water
 * */
@Service
public class WaterCalculatorService {

    /** Landscape can't have more than 32000 spaces
     * and each space must contain an int smaller than 32000 */
    public static final int MAX_NUMBER_OF_SPACES_AND_HEIGHT = 32000;

    /**
     * @param startingIndex starting positon in the landscape
     * @param landscape the landscape to check
     * @return the position in the landscape of the next "left bank" at or after the startingIndex
     */
    public int findNextLeftBankOfLake(int startingIndex, int[] landscape) {
        int i = startingIndex;

        while( i < (landscape.length - 1) ){
            // If current height is higher than next one
            // it means a "lake" is starting
            if(thereIsADecline(landscape[i], landscape[i+1])){
                return i;
            }
            i++;
        }

        return -1;
    }

    /**
     * Finds the next "right bank"  after the given "leftbank"
     *
     * It is not enough to find the next bank. But we should continue iterating
     * to find if this right bank is appropriate.
     * Because while we iterate over positions we might find a right-bank that
     * is higher than the previous, that makes it the correct bank
     * @param leftBank
     * @param landscape
     * @return integer value of the index in the array of the next right bank
     */
    public int findNextRightBankOfLake(int leftBank, int[] landscape) {
        int i = leftBank;
        boolean foundPossibleRightBank = false;
        int possibleRightBankIndex = -1;

        while( i < (landscape.length - 1)){
            if( thereIsAnIncline(landscape[i], landscape[i+1] )){
                if( foundPossibleRightBank ){
                    // check if lower than previously found
                    // and left bank is higher that possible right bank
                    if(( landscape[possibleRightBankIndex] < landscape[i+1] ) && ( landscape[leftBank] > landscape[possibleRightBankIndex] )){
                        possibleRightBankIndex = i+1;
                    }
                }else {
                    // first possible right bank
                    foundPossibleRightBank = true;
                    possibleRightBankIndex = i+1;
                }
            }
            i++;
        }

        return possibleRightBankIndex;
    }

    /**
     * @param leftBank index of left bank in the landscape array
     * @param rightBank index of right bank in the landscape array
     * @param landscape landscape with heights
     * @return a "lake"'s water amount
     */
    public int calculateSingleLakeWaterAmount(int leftBank, int rightBank, int[] landscape) {
        // iterate over range, calculate and sum values
        return IntStream.range(leftBank + 1, rightBank)
            .map(i -> getAmountOfWaterAtThisIndex(i, leftBank, rightBank,landscape))
            .sum();
    }

    /**
     * @param i
     * @param leftBank
     * @param rightBank
     * @param landscape
     * @return
     */
    private int getAmountOfWaterAtThisIndex(int i, int leftBank, int rightBank, int[] landscape) {
        int maxPossibleAmount = Math.min(landscape[leftBank], landscape[rightBank]);

        int waterAmountAtIndex = maxPossibleAmount - landscape[i];

        if(waterAmountAtIndex < 0){
            waterAmountAtIndex = 0;
        }
        return waterAmountAtIndex;
    }

    /**
     * @param previousHeight
     * @param currentHeight
     * @return
     */
    private boolean thereIsADecline(int previousHeight, int currentHeight) {
        return (previousHeight - currentHeight > 0);
    }

    /**
     * @param previousHeight
     * @param currentHeight
     * @return
     */
    private boolean thereIsAnIncline(int previousHeight, int currentHeight) {
        return (currentHeight - previousHeight > 0);
    }

    /**
     * @param landscape the landscape to check
     * @return the position in the landscape of the next "left bank" at or after the startingIndex
     */
    public void validateLandscape(int[] landscape) {
        if(landscape.length > MAX_NUMBER_OF_SPACES_AND_HEIGHT){
            throw new IllegalArgumentException("Maximum amount of spaces is 32000");
        }
        // iterate, filter to get first invalid value
        OptionalInt landscapeOptionalIncorrectNumber = Arrays.stream(landscape)
            .filter(height -> (height > MAX_NUMBER_OF_SPACES_AND_HEIGHT || height < 0))
            .findFirst();

        if(landscapeOptionalIncorrectNumber.isPresent()){
            throw new IllegalArgumentException("All contents of landscape have to be between 0 - " + MAX_NUMBER_OF_SPACES_AND_HEIGHT);
        }
    }

    /** Convert a String to int[] */
    public int[] convertStringToArrayOfInt(String landscape){
        try {

            String[] landscapeStringArray = landscape.trim().split(",");
            return Arrays.stream(landscapeStringArray)
                .mapToInt(Integer::parseInt)
                .toArray();

        }catch (Exception e){
            throw new IllegalArgumentException("Can't convert \'"+landscape +"\' to an integer array");
        }
    }
}

