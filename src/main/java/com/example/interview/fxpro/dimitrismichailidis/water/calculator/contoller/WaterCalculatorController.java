package com.example.interview.fxpro.dimitrismichailidis.water.calculator.contoller;


import com.example.interview.fxpro.dimitrismichailidis.water.calculator.IWaterCalculator;
import com.example.interview.fxpro.dimitrismichailidis.water.calculator.entity.Landscape;
import com.example.interview.fxpro.dimitrismichailidis.water.calculator.entity.ResponseDTO;
import com.example.interview.fxpro.dimitrismichailidis.water.calculator.service.WaterCalculatorService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/** Controller to allow getting the amount of water of a given landscape via API call */
@RestController
@RequestMapping("/api/v1")
public class WaterCalculatorController {

    @Autowired
    IWaterCalculator waterCalculator;

    @Autowired
    WaterCalculatorService waterCalculatorService;

    /** @param landscape Json value with "content": "< A csv format of integers>
     * @return Long number with water amount
     * @return 400 error in case of failed validation or similar that includes error message */
    @PostMapping("/waterCalculator")
    public ResponseEntity<ResponseDTO> PostMapping(@Valid @NotNull @RequestBody Landscape landscape)
    {
        try {
            int[] landscapeAsIntArray = waterCalculatorService.convertStringToArrayOfInt(landscape.getContent());

            long waterAmount = waterCalculator.calculateWaterAmount(landscapeAsIntArray);

            return ResponseEntity.ok(new ResponseDTO(waterAmount));

        }catch (IllegalArgumentException e){
            return ResponseEntity.badRequest().body(new ResponseDTO(e.getMessage()));
        }
    }

}
