package com.example.interview.fxpro.dimitrismichailidis.water.calculator.entity;

/** Entity class to represent a landscape
 * Used as input parameter in the exposed rest API */
public class Landscape {

    /**
     * A comma-separated string that represents the landscape values
     * */
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
